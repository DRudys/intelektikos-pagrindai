(deftemplate fragment (slot from) (slot to))
(deftemplate car (slot location))
(deftemplate obstacles (slot location) (slot t_lights) (slot cars) (slot pedestrians) (slot spec_service))

(deffacts faktu-inicializavimas
  (fragment (from "A") (to "B"))
  (fragment (from "B") (to "C"))
  (fragment (from "C") (to "D"))
  (fragment (from "D") (to "E"))
  (fragment (from "E") (to "F"))
  (fragment (from "F") (to "G"))
  (car (location "A"))
  (obstacles (location "C") (t_lights 3) (cars 2) (pedestrians 3) (spec_service 0))
  (obstacles (location "E") (t_lights 2) (cars 0) (pedestrians 0) (spec_service 0))
  (obstacles (location "F") (t_lights 0) (cars 0) (pedestrians 1) (spec_service 2))
)

(defrule r1 "Praleisti, jei atkarpoje yra automobiliu"
  (declare (salience 10))
  ?fact-id1 <- (car (location ?car_location))
  ?fact-id2 <- (obstacles (location ?obstacle_location) (t_lights ?t_lights) (cars ?cars) (pedestrians ?pedestrians) (spec_service ?spec_service))
  (test (eq ?car_location ?obstacle_location))
  (test (> ?cars 0))
  =>
  (printout t "Praleisti automobiliai" crlf)
  (modify ?fact-id2 (cars 0))
)

(defrule r2 "Praleisti, jei atkarpoje yra pesciuju"
  (declare (salience 9))
  ?fact-id1 <- (car (location ?car_location))
  ?fact-id2 <- (obstacles (location ?obstacle_location) (t_lights ?t_lights) (cars ?cars) (pedestrians ?pedestrians) (spec_service ?spec_service))
  (test (eq ?car_location ?obstacle_location))
  (test (> ?pedestrians 0))
  =>
  (printout t "Praleisti pestieji" crlf)
  (modify ?fact-id2 (pedestrians 0))
)
(defrule r3 "Praleisti, jei atkarpoje yra spec transporto"
  (declare (salience 8))
  ?fact-id1 <- (car (location ?car_location))
  ?fact-id2 <- (obstacles (location ?obstacle_location) (t_lights ?t_lights) (cars ?cars) (pedestrians ?pedestrians) (spec_service ?spec_service))
  (test (eq ?car_location ?obstacle_location))
    (test (> ?spec_service 0))
  =>
  (printout t "Praleistas spec transportas" crlf)
  (modify ?fact-id2 (spec_service 0))
)
(defrule r4 "Jei sviesaforas ne zalias - laukiama"
  (declare (salience 7))
  ?fact-id1 <- (car (location ?car_location))
  ?fact-id2 <- (obstacles (location ?obstacle_location) (t_lights ?t_lights))
  (test (eq ?car_location ?obstacle_location))
  (test (> ?t_lights 0))
  =>
  (printout t "Laukiama zalios sviesos" crlf)
  (modify ?fact-id2 (t_lights (- ?t_lights 1)))
)

(defrule r5 "Vaziuoti, jei atkarpoje nera kliuciu"
  ?fact-id1 <- (car (location ?car_location))
  ?fact-id2 <- (obstacles (location ?obstacle_location) (t_lights ?t_lights) (cars ?cars) (pedestrians ?pedestrians) (spec_service ?spec_service))
  ?fact-id3 <- (fragment (from ?from) (to ?to))
  (test (eq ?car_location ?from))
  =>
  (printout t "Automobilis vaziuoja toliau" crlf)
  (modify ?fact-id1 (location ?to))
)

(defrule r6 "Kai automobilis pasiekia galutini taska"
  ?fact-id1 <- (car (location ?car_location))
  ?fact-id2 <- (fragment (from ?from) (to ?to))
  (test (eq ?car_location ?to))
  =>
  (printout t "Pasiektas galutinis taskas" crlf)
)