(deftemplate pele (slot spalva) (slot kiekis) )
(deftemplate katino (slot busena) (slot suvalgyta_peliu) (slot dienu_nevalges))

(deffacts faktu-inicializavimas
  (pele (spalva pilka) (kiekis 5))
  (pele (spalva balta) (kiekis 3))
  (katino (busena "alkanas") (suvalgyta_peliu 0) (dienu_nevalges 0))
)

(defrule r1 "Kai katinas alkanas, jis nori valgyti"
  ?fact-id <- (katino (busena ?busena))  
  (test (eq ?busena "alkanas"))
  =>
  (modify ?fact-id (busena "nori valgyti"))
)
(defrule r2 "Kai katinas nori valgyti ir yra peliu, jis valgo peles"
  ?fact-id1 <- (katino (busena "nori valgyti") (suvalgyta_peliu ?suvalgyta) (dienu_nevalges ?nevalges))
  ?fact-id2 <- (pele (spalva ?spalva) (kiekis ?kiekis))
  (test (> ?kiekis 0))
  =>
  
  (if (eq ?spalva balta) then (printout t "py-py!" crlf)
                         else (printout t "pyyyyy" crlf))
  (modify ?fact-id2 (kiekis (- ?kiekis 1))  )
  
  (modify ?fact-id1 (suvalgyta_peliu (+ ?suvalgyta 1)) (dienu_nevalges 0)) 
  (printout t "miau" crlf)
)

(defrule r3 "kai katinas suvalgo 5 peles, jis tampa storu katinu"
  (declare (salience 10))
  ?fact-id1 <- (katino (busena "nori valgyti") (suvalgyta_peliu ?suvalgyta))
  (test (= ?suvalgyta 5)) 
  
=>
  (modify ?fact-id1 (busena "storas"))
)

(defrule r4 "Storas katinas miega ir pamirsta kiek peliu suvalge"
	?fact-id <- (katino (busena ?busena) (suvalgyta_peliu ?suvalgyta))
	(test (= ?suvalgyta 5)) 
  
=>
  (modify ?fact-id (busena "miega"))
)

(defrule r5 "Pamiegojes katinas pabunda alkanas"
	(declare (salience 3))
	?fact-id <- (katino (busena ?busena) (suvalgyta_peliu ?suvalgyta))
	(test (eq ?busena "miega")) 
  
=>
  (modify ?fact-id (busena "alkanas") (suvalgyta_peliu 0))
)
(defrule r6 "Kai katinas nori valgyti bet nera peliu jis badauja"
  (declare (salience -1))
  ?fact-id1 <- (katino (busena ?busena) (suvalgyta_peliu ?suvalgyta) (dienu_nevalges ?nevalges))
  ?fact-id2 <- (pele (spalva ?spalva) (kiekis ?kiekis))
  (test (< ?kiekis 1))
  (test (eq ?busena "nori valgyti"))
  =>
  
  (modify ?fact-id1 (dienu_nevalges (+ ?nevalges 1))) 
  (printout t "Siandien badauju" crlf)
)
(defrule r7 "Badaves savaite katinas pabega"
  ?fact-id1 <- (katino (busena ?busena) (suvalgyta_peliu ?suvalgyta) (dienu_nevalges ?nevalges))
  (test (= ?nevalges 7))
  (test (eq ?busena "nori valgyti"))
  =>
  
  (modify ?fact-id1 (busena "pabeges"))
)
(defrule r8 "Pabeges katinas nedaro nieko"
  (declare (salience 2))
  ?fact-id1 <- (katino (busena ?busena) (suvalgyta_peliu ?suvalgyta) (dienu_nevalges ?nevalges))
  (test (eq ?busena "pabeges"))
  =>
  
  (printout t "Katinas pabego" crlf)
)