clear
close all
sunspot = load('sunspot.txt');
figure(1);
plot(sunspot(:,1), sunspot(:,2), 'r-*')
title('Saul�s d�mi� skai�iaus priklausomyb� nuo met�')
xlabel('Metai')
ylabel('D�mi� skai�ius')

L = length(sunspot);
P = [sunspot(1:L-2,2)';
     sunspot(2:L-1,2)'];
T = sunspot(3:L,2)';

figure(2);
plot3(P(1,:), P(2,:), T(:), 'bo')
grid
title('D�mi� skai�iaus priklausomyb� nuo prie� tai dvejus metus buvusi� d�mi� skai�i�')
xlabel('D�mi� skai�ius x-2 metais'); ylabel('D�mi� skai�ius x-1 metais'); zlabel('D�mi� skai�ius x metais');

Pu = P(:,1:200);
Tu = T(1:200);

net = newlind(Pu, Tu);
disp('Neurono svorio koeficientai:')
disp(net.IW{1})
disp(net.b{1})
w1 = net.IW{1}(1);
w2 = net.IW{1}(2);
b = net.b{1};

Tsu = sim(net,Pu);
figure(3);
hold on
plot3(Pu(1,:), Pu(2,:), Tu(:), 'bo'); grid on
plot3(Pu(1,:), Pu(2,:), Tsu(:), 'ro'); grid on
legend('Tikrieji rezultatai', 'Prognozuojami rezultatai')
xlabel('D�mi� skai�ius x-2 metais'); ylabel('D�mi� skai�ius x-1 metais'); zlabel('D�mi� skai�ius x metais');

Ts = sim(net,P);
figure(4);
hold on
plot3(P(1,:), P(2,:), T(:), 'bo'); grid on
plot3(P(1,:), P(2,:), Ts(:), 'ro'); grid on
legend('Tikrieji rezultatai', 'Prognozuojami rezultatai')
xlabel('D�mi� skai�ius x-2 metais'); ylabel('D�mi� skai�ius x-1 metais'); zlabel('D�mi� skai�ius x metais');

e = T-Ts;
figure(5);
plot(1702:2014, e, 'r-*')
title('Sp�jimo klaidos e reik�m�s priklausomyb� nuo met�')
xlabel('Metai')
ylabel('Klaidos reik�m�')

figure(6);
hist(e)
title('Prognozes klaidu histograma');
xlabel('Klaidos reik�me');
ylabel('Da�nis');
vidutinisKvadratinisNuokrypis = mse(net,T,Ts,e)