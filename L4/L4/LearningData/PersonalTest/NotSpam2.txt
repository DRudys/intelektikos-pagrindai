Hi Dovydas,
Way to go in February! It's great to see that you've been tracking your workouts, so keep it up in March.

Did you know that having friends on Endomondo can motivate you to get even more active? You might already know some people on Endomondo, so check out which friends you can connect with now to have more fun getting fit.
Free your endorphins! 
Your Endomondo Team