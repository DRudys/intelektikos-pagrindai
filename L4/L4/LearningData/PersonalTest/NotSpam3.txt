
Hi,

An unusual activity was recently registered on your Ubisoft account for the following email address dovis119977@gmail.com. Please review the details below

Activity Login
Country Russia
IP address 91.189.160.41

If this was you

Great! You can safely ignore this email.


If this wasn't you

Your Ubisoft account may have been compromised and you should take a few steps to make sure it is secure. To start, reset your password now. After that, we recommend you to activate the 2-Step Verification to enhance the security of your account.


Need additional help Contact Ubisoft Customer Support.

Ubisoft