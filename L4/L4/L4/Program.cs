﻿using System.IO;

namespace L4
{
    class Program
    {
        static string learningDirectory = Directory.GetCurrentDirectory() + "/../../../LearningData";

        static string filesToAnalyze = Directory.GetCurrentDirectory() + "/../../../LearningData/PersonalTest";

        static void Main(string[] args)
        {
            SpamClassifier classifier = new SpamClassifier(learningDirectory);

            //classifier.Learn();

            foreach (var file in Directory.GetFiles(filesToAnalyze))
            {
                classifier.Analyze(file, 20, (decimal)0.7, (decimal)0.5);
            }
        }
    }
}
