﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L4
{
    class Frequency
    {
        public int Count { get; set; }
        public string Word { get; set; }
        public decimal Probability { get; set; }

        public Frequency(string word)
        {
            Word = word.ToLower();
            Count = 1;
        }

        public void Increase()
        {
            Count++;
        }

        public void SetProbability(decimal prob)
        {
            Probability = prob;
        }

        public override bool Equals(object obj)
        {
            return Word == ((Frequency)obj).Word;
        }

        public override int GetHashCode()
        {
            return Word.GetHashCode();
        }
    }

    class SpamClassifier
    {
        static List<char> separators = Separators();
        string SpamDirectory;
        string NotSpamDirectory;
        string CalculatedData;

        public SpamClassifier(string learningDirectory)
        {
            SpamDirectory = learningDirectory + "/Spam";
            NotSpamDirectory = learningDirectory + "/NotSpam";
            CalculatedData = learningDirectory + "/Calculated.txt";
        }

        public void Analyze(string file, int lexemsToAnalyze, decimal limitWhenConsideredSpam, decimal probabilityForNewWords)
        {
            List<string> probabilities = File.ReadAllLines(CalculatedData).ToList();
            Dictionary<string, decimal> probs = new Dictionary<string, decimal>();
            foreach (string prob in probabilities)
            {
                string[] arr = prob.Split(' ');
                probs.Add(arr[0], Decimal.Parse(arr[1]));
            }

            List<string> words = SplitFileToWords(file);
            Dictionary<string, decimal> newProbs = new Dictionary<string, decimal>();
            foreach (string word in words)
            {
                if (!newProbs.ContainsKey(word))
                {
                    if (probs.ContainsKey(word))
                    {
                        KeyValuePair<string, decimal> found = probs.FirstOrDefault(prob => prob.Key == word);
                        newProbs.Add(found.Key, found.Value);
                    }
                    else
                    {
                        newProbs.Add(word, probabilityForNewWords);
                    }
                }
            }
            newProbs = newProbs.OrderByDescending(prob => Math.Abs(prob.Value - (decimal)0.5)).ToDictionary(x => x.Key, x => x.Value);

            decimal a = 1;
            decimal b = 1;

            foreach (var x in newProbs.Take(Math.Min(lexemsToAnalyze, newProbs.Count)))
            {
                a *= x.Value;
                b *= (1 - x.Value);
            }

            decimal p = a / (a + b);

            decimal percents = Math.Round(p * 100, 2);

            string[] filePath = file.Split(new char[] { '\\', '/' });
            string fileName = filePath[filePath.Length - 1];
            Console.WriteLine("{0} - {1}, {2}%", fileName, p >= limitWhenConsideredSpam ? "Spam" : "Not spam", percents);
        }

        public void Learn()
        {
            string[] spamFiles = Directory.GetFiles(SpamDirectory);
            string[] notSpamFiles = Directory.GetFiles(NotSpamDirectory);

            HashSet<Frequency> frequenciesSpam = new HashSet<Frequency>();
            HashSet<Frequency> frequenciesNotSpam = new HashSet<Frequency>();

            List<string> distinctWordsInSpam = new List<string>();
            List<string> distinctWordsInNotSpam = new List<string>();
            List<string> allDistinctWords = new List<string>();


            Task t1 = Task.Factory.StartNew(() => { FindProbabilitiesInFiles(spamFiles, frequenciesSpam, out distinctWordsInSpam); });
            Task t2 = Task.Factory.StartNew(() => { FindProbabilitiesInFiles(notSpamFiles, frequenciesNotSpam, out distinctWordsInNotSpam); });
            t1.Wait();
            t2.Wait();

            allDistinctWords.AddRange(distinctWordsInSpam);
            allDistinctWords.AddRange(distinctWordsInNotSpam);
            allDistinctWords = allDistinctWords.Distinct().ToList();

            FindFinalProbabilities(CalculatedData, frequenciesNotSpam, frequenciesSpam, allDistinctWords);
        }

        static void FindFinalProbabilities(string resultsFile, HashSet<Frequency> notSpam, HashSet<Frequency> spam, List<string> allDistinctWords)
        {
            if (File.Exists(resultsFile))
                File.Delete(resultsFile);

            using (StreamWriter sw = File.AppendText(resultsFile))
            {
                foreach (string word in allDistinctWords)
                {
                    decimal a = 0;
                    decimal b = 0;

                    Frequency temp = new Frequency(word);
                    if (spam.Contains(temp))
                        a = spam.First(x => x.Equals(temp)).Probability;
                    if (notSpam.Contains(new Frequency(word)))
                        b = notSpam.First(x => x.Equals(temp)).Probability;

                    decimal finalProbability;

                    if (b == 0)
                        finalProbability = (decimal)0.99;
                    else if (a == 0)
                        finalProbability = (decimal)0.01;
                    else
                        finalProbability = a / (a + b);

                    sw.WriteLine("{0} {1}", word, finalProbability);
                }
            }
        }

        static void FindProbabilitiesInFiles(string[] files, HashSet<Frequency> frequencies, out List<string> wordsInFolder)
        {
            int wordsInFolderCount = 0;
            wordsInFolder = new List<string>();
            foreach (string file in files)
            {
                List<string> fileWords;
                CountFrequenciesInFile(file, frequencies, out fileWords);
                wordsInFolderCount += fileWords.Count;
                wordsInFolder.AddRange(fileWords.Distinct());
            }
            wordsInFolder = wordsInFolder.Distinct().ToList();

            foreach (Frequency freq in frequencies)
            {
                freq.SetProbability((decimal)freq.Count / wordsInFolderCount);
            }
        }

        static void CountFrequenciesInFile(string file, HashSet<Frequency> frequencies, out List<string> words)
        {
            words = SplitFileToWords(file);

            HashSet<Frequency> list = frequencies;
            foreach (string word in words)
            {
                Frequency temp = new Frequency(word);
                if (list.Contains(temp))
                {
                    list.First(x => x.Equals(temp)).Increase();
                }
                else
                {
                    list.Add(new Frequency(word));
                }
            }
        }

        static List<string> SplitFileToWords(string file)
        {
            string text = System.IO.File.ReadAllText(file);

            return text.Split(separators.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        static List<char> Separators()
        {
            List<Char> separators = new List<char>();
            for (int i = char.MinValue; i <= char.MaxValue; i++)
            {
                char c = Convert.ToChar(i);
                separators.Add(c);
            }
            string specChars = "$'\"";

            separators.RemoveAll(x => (x >= 'a' && x <= 'z') || (x >= 'A' && x <= 'Z') || (x >= '0' && x <= '9') || specChars.Contains(x));
            return separators;
        }
    }
}
